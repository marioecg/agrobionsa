import Swiper from 'swiper/dist/js/swiper'
import $ from 'jquery/dist/jquery'
import SmoothScroll from 'smooth-scroll.js/dist/smooth-scroll'
import ScrollReveal from 'scrollreveal/dist/scrollreveal'
import download from 'downloadjs/download'

window.sr = ScrollReveal({
  // Time in milliseconds.
  duration: 900,
  delay: 250,

  // Starting scale value, will transition from this value to 1
  scale: 0
})

sr.reveal('.c-hero')

sr.reveal('.c-section')

sr.reveal('.js-sr')


// Creating variables
var hamburger = document.querySelector('.c-hamburger')
var mobileNav = document.querySelector('.c-mobile-nav')
var allBtn = document.querySelector('#btn-all')
var prod1Btn = document.querySelector('#btn-bioins')
var prod2Btn = document.querySelector('#btn-biofun')
var prod3Btn = document.querySelector('#btn-bionem')
var prod4Btn = document.querySelector('#btn-ecoins')
var allContent = document.querySelector('#all')
var prod1Content = document.querySelector('#bioins')
var prod2Content = document.querySelector('#biofun')
var prod3Content = document.querySelector('#bionem')
var prod4Content = document.querySelector('#ecoins')
var arr1 = [allBtn, prod1Btn, prod2Btn, prod3Btn, prod4Btn]
var arr2 = [allContent, prod1Content, prod2Content, prod3Content, prod4Content]
var select = document.querySelector('.c-select')
var acc = document.querySelectorAll('.c-accordion__name')
var faq = document.querySelectorAll('.c-faq__title')
var subfaq = document.querySelectorAll('.c-subfaq__title')

// Smooth Scroll
var scroll = new SmoothScroll('a[href*="#"]')

// Swiper
var mySwiper = new Swiper('.swiper-container', {
  // Optional parameters
  direction: 'horizontal',
  loop: true,

  // If we need pagination
  pagination: {
    el: '.swiper-pagination',
  },

  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },

  centeredSlides: 'true'
})

// Make hamburger change when clicked
hamburger.addEventListener('click', function () {
  hamburger.classList.toggle('burger-is-active')
  mobileNav.classList.toggle('nav-is-active')
})

// Changes header background color when scrolled
$(document).ready(function () {

  var scrollStart = 0
  var startchange = $('#header')
  var offset = startchange.offset()

  if (startchange.length) {
    $(document).scroll(function () {

      scrollStart = $(this).scrollTop()

      if (scrollStart > offset.top) {

        $('.c-header').addClass('is-colored')

      } else {

        $('.c-header').removeClass('is-colored')

      }
    })
  }
})

// Product switcher
function switcher () {
  if (document.querySelector('.c-products__container')) {

    // Show all products
    allBtn.addEventListener('click', function () {
      for (var i = 0; i < arr1.length; i++) {
        arr1[i].classList.remove('is-active')
      }

      for (var i = 0; i < arr2.length; i++) {
        arr2[i].classList.remove('is-active')
      }

      allBtn.classList.add('is-active')
      allContent.classList.add('is-active')
    })

    // Show bioinsecticide products
    prod1Btn.addEventListener('click', function () {
      for (var i = 0; i < arr1.length; i++) {
        arr1[i].classList.remove('is-active')
      }

      for (var i = 0; i < arr2.length; i++) {
        arr2[i].classList.remove('is-active')
      }

      prod1Btn.classList.add('is-active')
      prod1Content.classList.add('is-active')
    })

    // Show biofungicide products
    prod2Btn.addEventListener('click', function () {
      for (var i = 0; i < arr1.length; i++) {
        arr1[i].classList.remove('is-active')
      }

      for (var i = 0; i < arr2.length; i++) {
        arr2[i].classList.remove('is-active')
      }

      prod2Btn.classList.add('is-active')
      prod2Content.classList.add('is-active')
    })

    // Show bionematicides products
    prod3Btn.addEventListener('click', function () {
      for (var i = 0; i < arr1.length; i++) {
        arr1[i].classList.remove('is-active')
      }

      for (var i = 0; i < arr2.length; i++) {
        arr2[i].classList.remove('is-active')
      }

      prod3Btn.classList.add('is-active')
      prod3Content.classList.add('is-active')
    })

    // Show ecoinsecticides products
    prod4Btn.addEventListener('click', function () {
      for (var i = 0; i < arr1.length; i++) {
        arr1[i].classList.remove('is-active')
      }

      for (var i = 0; i < arr2.length; i++) {
        arr2[i].classList.remove('is-active')
      }

      prod4Btn.classList.add('is-active')
      prod4Content.classList.add('is-active')
    })
  }
}

// Product select box for mobile
function selector () {

  if (document.querySelector('.c-select')) {

    // Watch selector when value changes
    select.addEventListener('change', function () {

      // Select all products
      if (select.value === '0') {
        for (var i = 0; i < arr1.length; i++) {
          arr1[i].classList.remove('is-active')
        }

        for (var i = 0; i < arr2.length; i++) {
          arr2[i].classList.remove('is-active')
        }

        allContent.classList.add('is-active')
      }

      // Select bioinscticides
      if (select.value === '1') {
        for (var i = 0; i < arr1.length; i++) {
          arr1[i].classList.remove('is-active')
        }

        for (var i = 0; i < arr2.length; i++) {
          arr2[i].classList.remove('is-active')
        }

        prod1Content.classList.add('is-active')
      }

      // Select biofungicides
      if (select.value === '2') {
        for (var i = 0; i < arr1.length; i++) {
          arr1[i].classList.remove('is-active')
        }

        for (var i = 0; i < arr2.length; i++) {
          arr2[i].classList.remove('is-active')
        }

        prod2Content.classList.add('is-active')
      }

      // Select bionematicides
      if (select.value === '3') {
        for (var i = 0; i < arr1.length; i++) {
          arr1[i].classList.remove('is-active')
        }

        for (var i = 0; i < arr2.length; i++) {
          arr2[i].classList.remove('is-active')
        }

        prod3Content.classList.add('is-active')
      }

      // Select all eco insecticides
      if (select.value === '4') {
        for (var i = 0; i < arr1.length; i++) {
          arr1[i].classList.remove('is-active')
        }

        for (var i = 0; i < arr2.length; i++) {
          arr2[i].classList.remove('is-active')
        }

        prod4Content.classList.add('is-active')
      }
    })
  }
}

function collapse () {
  for (var i = 0; i < acc.length; i++) {
    acc[i].addEventListener('click', function () {
      this.classList.toggle('is-active')
      var panel = this.nextElementSibling
      if (panel.style.display === 'block') {
        panel.style.display = 'none'
      } else {
        panel.style.display = 'block'
      }
    })
  }
}

function faqCollapse () {
  for (var i = 0; i < faq.length; i++) {
    faq[i].addEventListener('click', function () {
      this.classList.toggle('is-active')
      var panel = this.nextElementSibling
      if (panel.style.display === 'block') {
        panel.style.display = 'none'
      } else {
        panel.style.display = 'block'
      }
    })
  }
}

function subfaqCollapse () {
  for (var i = 0; i < subfaq.length; i++) {
    subfaq[i].addEventListener('click', function () {
      this.classList.toggle('is-active')
      var panel = this.nextElementSibling
      if (panel.style.display === 'block') {
        panel.style.display = 'none'
      } else {
        panel.style.display = 'block'
      }
    })
  }
}

switcher()
selector()
collapse()
faqCollapse()
subfaqCollapse()

// open pop up form when download button is clicked
var popup = document.querySelector('.c-form__container')
var overlay = document.querySelector('.overlay')

function openForm () {
  popup.classList.add('c-form__container--open')
  overlay.classList.add('overlay--open')
}

// close form when x clicked
var close = document.querySelector('.c-form__close')

close.addEventListener('click', function () {

  popup.classList.remove('c-form__container--open')
  overlay.classList.remove('overlay--open')
})

// get download file according to button clicked
var submit = document.querySelector('.c-btn--popup')
var file

function loadFile () {
  file = this.dataset.file
}

function downloadFile () {
  download(file)
}

var buttons = document.querySelectorAll('.c-btn--downloads')
buttons.forEach(button => button.addEventListener('click', openForm))
buttons.forEach(button => button.addEventListener('click', loadFile))

submit.addEventListener('click', downloadFile)

